package humanos;

public abstract class Persona implements Humano {

  private int edad;
  protected String nombre ;
  protected Cabellos cabello = new Cabellos(); 
  
  public Persona(String nombre, String color, String largo){
    this.nombre = nombre; 
    
    this.cabello.color = color;
    this.cabello.largo = largo;
  }
  private int calcularAños( int añoNacimiento ) {
      return 2018 - añoNacimiento; 
  }
  public int edad(int añoNacimiento){
     this.edad = calcularAños(añoNacimiento);
     System.out.println("Tengo " + this.edad + " años");
     return  this.edad;
  }
  public void setNombre(String nombre){
      this.nombre = nombre; 
  }
  public String getNombre(){
      return this.nombre; 
  }
  
}