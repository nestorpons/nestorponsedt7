package humanos;

public class Mujer extends Persona {

  public Bolso  tieneBolso;
    /**
   * 
   * @element-type bolso
   */
  
  public Mujer(String nombre, String color, String largo){
    super(nombre, color, largo);
    
    System.out.println("Hola yo soy una " + CHICA);
    tieneBolso = new Bolso();
    tieneBolso.color = "Rojo";
    tieneBolso.llenar();
  }
  @Override
  public void mear() {
     System.out.println("Y yo meo sentada");
  }

}