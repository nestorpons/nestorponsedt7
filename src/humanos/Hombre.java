package humanos;

public class Hombre extends Persona implements Empleado{
    private String empleo = "Bombero-Torero";
    
    public Hombre(String nombre, String color, String largo) {
      super(nombre,color,largo); 
      
      System.out.println("Hola yo soy un " + CHICO );      
    }
    
    @Override
    public void mear() {
       System.out.println("Y yo meo de pie");
    }

    @Override
    public String empleo() {
      return empleo; 
    }

    @Override
    public void cambiarEmpleo(String nuevoEmpleo) {
        this.empleo = nuevoEmpleo; 
    }

}